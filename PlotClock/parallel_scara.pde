class Scara extends Point{
  
 private Cheese cs;
 private final static float RAD2DEG = 57.29577951;
 private final static float DEG2RAD = 0.017453292;
 
 //scara parameter
 private final static float leftArm1 = 34;  //mm
 private final static float leftArm2 = 49.8;  //mm
 private final static float rightArm1 = 34.85;  //mm
 private final static float rightArm2 = 55.2;  //mm
 private final static float armDist = 26.8;  //mm
 
 //scara variance
 private int cali_Sa=0, cali_Sb=0, cali_Sc=0;
 private int leftServoAngle, rightServoAngle;
 private int pre_leftServoAngle, pre_rightServoAngle;
  
 private int tic = -1;
 private int writtenFlag = 0;
 private boolean didJump = false;
 private boolean getEraser = false;
 private boolean didErase = false;
 private int erase_state = 1;
 private int eraserLocation = 1;  // 1: right, 0 : left
 private int eraserX= 38, eraserY=60;  // the position of end effector
   
 private final static int ERASER_NOT = 1;
 private final static int ERASER_GET = 2;
 private final static int ERASER_DONE = 3;

 Scara()
 {
    // None 
 }
 
 Scara(Cheese chs){
   this.cs = chs;
   //motor configuration
   
   cs.configSa(cs.SERVO_OUT,0);  //left arm
   cs.configSb(cs.SERVO_OUT,0);  //right arm
   cs.configSc(cs.SERVO_OUT,0);  //up-down motion
 }
 
 // overLoading
 public void Calibration(int Sa, int Sb, int Sc){
   cali_Sa = Sa;
   cali_Sb = Sb;
   cali_Sc = Sc;
   Output_S(90,90,0);
 }
 
 // overLoading
 public void Calibration(int Sa, int Sb){
   cali_Sa = Sa;
   cali_Sb = Sb;
   Output_S(90,90,0);
 }
 
 // overLoading
 private void Output_notCali(int Sa,int Sb,int Sc){
   cs.outSa = Sa;
   cs.outSb = Sb;
   cs.outSc = Sc;
 }
 
 // overLoading
 private void Output_notCali(int Sa,int Sb){
   cs.outSa = Sa;
   cs.outSb = Sb;
 }
 
 public void Output_S(int Sa, int Sb, int Sc){
   cs.outSa = (Sa + cali_Sa > 179 ? 179 : (Sa + cali_Sa < 1 ? 1 : Sa + cali_Sa));  // 1 < outSa < 179
   cs.outSb = (Sb + cali_Sb > 179 ? 179 : (Sb + cali_Sb < 1 ? 1 : Sb + cali_Sb));  // 1 < outSb < 179
   cs.outSc = (Sc + cali_Sc > 179 ? 179 : (Sc + cali_Sc < 1 ? 1 : Sc + cali_Sc));  // 1 < outSc < 179
 }
 
 public void Output_S(int Sa, int Sb){
   cs.outSa = (Sa + cali_Sa > 179 ? 179 : (Sa + cali_Sa < 1 ? 1 : Sa + cali_Sa));  // 1 < outSa < 179
   cs.outSb = (Sb + cali_Sb > 179 ? 179 : (Sb + cali_Sb < 1 ? 1 : Sb + cali_Sb));  // 1 < outSb < 179
 }
 
 private void InverseKinematics(float targetX, float targetY){  //Point(x,y) convert to arm angle(q1,q2)
   float zeta1 = 0, zeta2 = 0;
   float gamma1 = 0 , gamma2 = 0;
   float k1_sqr = SQR(targetX) + SQR(targetY);
   float k2_sqr = SQR(targetX - armDist) + SQR(targetY);
   float k1 = sqrt(k1_sqr), k2 = sqrt(k2_sqr);
   
   zeta1 = atan2(targetY,targetX) * RAD2DEG;
   zeta2 = atan2(targetY, targetX - armDist)* RAD2DEG;
   gamma1 = acos((SQR(leftArm1) + k1_sqr - SQR(leftArm2)) / (2*leftArm1*k1))* RAD2DEG;
   gamma2 = acos((SQR(rightArm1) + k2_sqr - SQR(rightArm2)) / (2*rightArm1*k2))* RAD2DEG;
   
   leftServoAngle = int(zeta1 + gamma1);
   rightServoAngle = int(zeta2 - gamma2);
   //println(rightServoAngle);  //이게 0보다 작은 값이 안나오는지 확인해보자
 }
 
 public void MoveArm2XY(float targetX, float targetY){
   int threshold = 20;
   InverseKinematics(targetX,targetY);
   
   Output_S(leftServoAngle,rightServoAngle);   // servo output
 }  //func
 
 // scara's coordinate system ( x : -5 ~ 35, y : 30 ~ 70 ) 
 public void Draw(int index, int num){
  float basePointX = -5.0, basePointY = 30.0;
   
   switch(index){  // set base point position
   case 1:
     basePointX = -5.0;
     break;
     
   case 2:
     basePointX = 7.5;
     break;
     
   case 3:
     basePointX = 20.0;
     break;
     
   case 4:
     basePointX = 32.5;
     break;
     
  }
   
   PenDown();
   
   tic++;
   switch(num){
    case 0:
      if(tic >= 0 && tic <= 47)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
       writtenFlag++;
       tic = -1; // reset
      }
      break;
      
    case 1:
      if(tic >= 0 && tic <= 18)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
      
    case 2:
      if(tic >= 0 && tic <= 39)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
      
    case 3:
      if(tic >= 0 && tic <= 37)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
      
    case 4:
      if(tic >= 0 && tic <= 47)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
      
    case 5:
      if(tic >= 0 && tic <= 40)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
      
    case 6:
      if(tic >= 0 && tic <= 39)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
      
    case 7:
      if(tic >= 0 && tic <= 34)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
      
    case 8:
      if(tic >= 0 && tic <= 43)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
      
    case 9:
      if(tic >= 0 && tic <= 34)
      {
        MoveArm2XY(basePointX + pointsX[num][tic],basePointY + pointsY[num][tic]);
      }
      else
      {
        didJump = false;
        writtenFlag++;
       tic = -1; 
      }
      break;
    } // switch
 } // draw
 
 public void DrawColon()
 {
   tic++;
   int toc = 7;
   if(tic < toc * 2)
   {
     PenUp();  
   }
   else if(tic < toc * 3)
   {
     MoveArm2XY(23,55);
   }
   else if(tic < toc * 4)
   {
     PenDown();  
   }
   else if(tic < toc * 5)
   {
     PenUp();
   }
   else if(tic < toc * 6)
   {
     MoveArm2XY(20,40);  
   }
   else if(tic < toc * 7)
   {
     PenDown();
   }
   else
   {
     PenUp();
     writtenFlag++;
     tic = -1;  // next step is writing number. so tic = -1
   }
 }
 
 public void Jump(int index, int num)
 {
   float basePointX = -5.0, basePointY = 30.0;
   
   tic++;
   if(tic < 5)
   {
     PenUp();
   }
   else if(tic < 10)
   {
    switch(index)  // set base point position
    {  
     case 1:
       basePointX = -5.0;
       break;
       
     case 2:
       basePointX = 7.5;
       break;
       
     case 3:
       basePointX = 20.0;
       break;
       
     case 4:
       basePointX = 32.5;
       break;
       
    }
     MoveArm2XY(basePointX + pointsX[num][0],basePointY + pointsY[num][0]);  // set end effector ready to write number    
   }
   else if(tic < 13)
   {
    PenDown();
   }
   else
   {
     tic = -1;
    didJump = true;
   }
 }
 
 public void PenUp()
 {
   cs.outSc = 75;
 }
 
 public void PenDown()
 {
   cs.outSc = 105;
 }
 
 public void EraserDown()
 {
   cs.outSc = 110;
 }
 
 public void EraserUp()
 {
  cs.outSc = 40; 
 }
 
 public void ResetWrittenFlag()
 {
   writtenFlag = 0;
 }
 public void ResetTic()
 {
  tic = 0 ; 
 }
 
 // 지우개 넣을때 좀더 멀리서부터 들어가야한다.
 private void SetEraser()
 {
   tic++;
   EraserDown();
  if(tic < 5)
  {
    MoveArm2XY(40,60);  // eraser ready to get in
  }else if(tic < 7)
  {
    MoveArm2XY(45,60);  // get eraser in
  }else if(tic < 9)
  {
    MoveArm2XY(50,60);  // get eraser in
  }else if(tic < 11)
  {
    MoveArm2XY(55,60);  // get eraser in
  }else if(tic < 13)
  {
    MoveArm2XY(58,60);  // get eraser in
  }else if(tic < 15)
  {
    MoveArm2XY(55,60);  //
  }else if(tic < 20)
  {
    MoveArm2XY(50,60);  
  }else if(tic < 25)
  {
    EraserUp();  // pen up
  }else if(tic < 100)
  {
    MoveArm2XY(30,45);  // wait 6 sec
  }
  else
  {
   tic = 0;
   erase_state = 1;
  } 
 }
 
 // 지우개 뺄 때 더 땡겨야된다.
 private void GetEraser()
 {
  tic++;
  if(tic < 5)
  {
    EraserUp();  // raise arm up
  }else if(tic < 10)
  {
    MoveArm2XY(48,61.5);  // eraser position : 47,59
  }else if(tic < 15)
  {
    EraserDown();
  }else if(tic < 20)
  {
    MoveArm2XY(20,60);  // take eraser out
  }
  else
  {
   tic = 0;
   erase_state = 2;  // take eraser out
   
   //setting for EraseAll()
   eraserLocation = 1;  // location : right side
   eraserX = 38;
   eraserY = 60;
  }
 } // GetEraser()
 
 private void EraseAll()
 {
  if(erase_state == 2)
  {
    if(eraserY <= 30)  // escape condition
    {
      erase_state = 3;
    }
    else  // erase all
    {
      EraserDown();
      
      int rangeRight = -10;
      int rangeLeft = 45;
      
      switch(eraserLocation)  // eraserLocation == 1 : right, eraserLocation == 2 : left
      {
       case 1:  // right -> left
         eraserX -= 2;    // eraser goes left
         if( eraserX > rangeRight)
         {
           MoveArm2XY(eraserX,eraserY);  // erase
         }else if(eraserX <= rangeRight)
         {
          eraserY -= 5;    // eraser down
          eraserLocation = 2;  //eraser left
         }
         break;
         
       case 2:  // left -> right
         eraserX += 2;    // eraser goes right
         if(eraserX < rangeLeft)
         {
          MoveArm2XY(eraserX,eraserY);  // erase
         }
         else if(eraserX >= rangeLeft)
         {
          eraserY -= 5;    // eraser down
          eraserLocation = 1;  //eraser right
         }
         break;
      }
    }
  }//if eraser_state == 1
  else
  {
   println("it doesn't have eraser."); 
  }
  
 } // EraseAll()
 
 public void EraseBoard()
 {
   switch(erase_state)
    {
      case ERASER_NOT :  // 1
        GetEraser();
        break;
      case ERASER_GET :  // 2
        EraseAll();
        break;
      case ERASER_DONE :  // 3
        SetEraser();
        break;
    }
 }
 
 public void ResetEraser()
 {
    erase_state = 1;  // get ready to execute getEraser()
 }
 
 public void ResetPen()
 {
    writtenFlag = 0;
    didJump = false;
 }
 
 protected float SQR(float x){
   return x*x;
 }
 
}  // class